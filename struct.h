/**
*\file struct.h
*\brief struct.h déclare les fonctions contenue dans le fichier function.c et struct.c et il déclare également toutes les structures uttilisé dans ce projet.
*\author Ludovic CASTIGLIA
*/

typedef struct Maillondemande{
    char nom[31];
    int numc,
        desdp,
        valcan;
        struct Maillondemande *prec;
} demande;

typedef struct MaillonDept{
    char nom[31];
    int nbp;
    char resp[31];
    struct MaillonDept *suiv;
    demande *dem;
} maillonDept,*ListeDept;

typedef struct {
    char ville[31];
    ListeDept ldept;
} VilleIUT;

typedef struct MaillonChoix{
    char ville[31],
        dep[31];
    int desdp,
        valcan;
    struct MaillonChoix *suiv;
} maillonChoix, *ListeChoix;

typedef struct {
    int numc;
    char nom[31];
    char prenom[31];
    float note[4];
    int nbchoix;
    ListeChoix lchoix;
} Candidat;




ListeDept ajouter_departement(ListeDept ldept,char nom[31], int nbp, char resp[31]);
ListeDept ajouter_departement_en_tete(ListeDept ldept,char nom[31], int nbp, char resp[31]);
ListeDept supprimer_departement(ListeDept ldept,char nom[31]);
ListeDept supprimer_departement_en_tete(ListeDept ldept);
VilleIUT** ajouter_ville(VilleIUT *tiut[], int *tl, char nom[31], int pos);
VilleIUT** decalage_a_droite(VilleIUT** tiut, int pos, int tl);
int recherche_ville(VilleIUT **tiut,char ville[31],int *trouve, int tl);
void maillon_cpy(maillonDept *a, maillonDept *b);
void affiche_villes(VilleIUT *tiut[], int tl);
void affiche_ListeDept(maillonDept *m);
int nb_place_departement(maillonDept *m, char dep[31]);
int departement_in_ville(maillonDept *m, char dep[31]);
ListeDept fil_nouv_ldept(void);
ListeChoix fil_nouv_lchoix(void);
void affiche_demande(demande *m);
void affiche_candidat(Candidat c);
void affiche_ListeChoix(ListeChoix m);
Candidat** ajouter_candidat(Candidat *tc[], int *tl, int numc, float note[4], char nom[31], char prenom[31], int nbchoix,int pos);
Candidat** decalage_a_droite_candidat(Candidat** tc, int pos, int tl);
int recherche_candidat(Candidat **tc, int numc,int tl, int *trouve);
ListeChoix ajouter_choix(ListeChoix lchoix,char ville[31], char dep[31], int desdp, int valcan);
ListeChoix ajouter_choix_en_tete(ListeChoix lchoix,char ville[31],char dep[31], int desdp, int valcan);
void affiche_tout_candidat(Candidat** tc, int tlc);
void affiche_un_candidat(Candidat** tc, int tlc);
void affiche_candidat_departement(VilleIUT *tiut[], int tl);
void affiche_candidat_departement_ldept(ListeDept ldept,char dep[31]);
void ajouter_demande_ldept(ListeDept ldept,char dep[31], char nom[31], int numc, int desdp, int valcan);
demande* ajouter_demande(demande *dem, char nom[31], int numc, int desdp, int valcan);
demande* ajouter_demande_en_tete(demande *dem, char nom[31], int numc, int desdp, int valcan);
void affiche_les_voeux(Candidat** tc, int tlc);
void ajouter_un_voeux(VilleIUT *tiut[], int tl, Candidat** tc, int tlc);
void supprimer_un_voeux(VilleIUT *tiut[], int tl, Candidat** tc, int tlc);
int demande_existe(ListeChoix l, char ville[31], char dep[31]);
void supprimer_demande_ldept(ListeDept ldept,char dep[31], char nom[31]);
demande* supprimer_demande(demande *dem, char nom[31]);
demande* supprimer_demande_en_tete(demande *dem);
ListeChoix supprimer_choix_en_tete(ListeChoix lchoix);
ListeChoix supprimer_choix(ListeChoix lchoix,char ville[31], char dep[31]);