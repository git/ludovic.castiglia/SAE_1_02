/**
*\file main.c
*\brief fichier principale qui permet le lancement du programe ainsi que la lecture et l'écriture des fichiers de données
*author Ludovic CASTIGLIA
*/


#include"struct.h"
#include"main.h"
#include"menu.h"

/**
\brief fonction principale qui lance la function globale
\param [out] int return 0 par défault.
\param [in] void void
\return 0
*/
int main(void)
{
    global();
    return 0;
}
/**
\brief fonction qui lit dans le fichier iut et charge le tableau tiut
\param [out] tiut tableau de pointeur vaire des structures VilleIUT possédant une liste chainé
\param [in] fichier chaine de charactaire représentant la direction du fichier iut
\param [in] tl pointeur représantant la taille logique du tableau tiut
\return tiut le tableau de pointeur vaire des structures VilleIUT possédant une liste chainé
*/
VilleIUT** lire_iut(char fichier[],int *tl)
{
    char ville[31], departement[31], nom[31];
    int nbpers, pos=0, trouve=0;
    VilleIUT **tiut ;
    FILE *file;
    file = fopen(fichier,"r");
    if (file == NULL) stop("erreur lors de la lecture dans lire_iut(function.c)");
    read_line_iut(ville,departement,&nbpers,nom,file);
    while(!feof(file))
    {
        if (*tl != 0) pos = recherche_ville(tiut,ville,&trouve, *tl);
        if (!trouve)
            tiut =  ajouter_ville(tiut, tl, ville, pos);
        tiut[pos]->ldept = ajouter_departement(tiut[pos]->ldept,departement,nbpers,nom);
        read_line_iut(ville,departement,&nbpers,nom,file);
    }
    fclose(file);
    return tiut;
}
/**
\brief sauvegarde les données contenu dans le tableau tiut dans le fichier iut en libérant la meimoir
\param [out] void void
\param [in] tiut tableau de pointeur vaire des structures VilleIUT possédant une liste chainé
\param [in] tl taille logique du tableau tiut
\return void
*/
void save_iut(VilleIUT *tiut[], int tl)
{
    FILE *file;
    file = fopen("./fichier/iut","w");
    if (file == NULL) stop("erreur lors de l'écriture dans save_iut(main.c)");
    while(tl>0)
    {
        tl-=1;
        while(tiut[tl]->ldept != NULL)
        {
            save_line_iut(file,tiut[tl]->ldept,tiut[tl]->ville);
            tiut[tl]->ldept = supprimer_departement_en_tete(tiut[tl]->ldept);
        }
    }
    fclose(file);
}
/**
\brief ecrit dans le fichier passer en paramètre les informations concernant un departement eux aussi passer en paramètre
\param [out] void void
\param [in] file pointeur de type FILE correspond au fichier iut
\param [in] ldept pointeur de type maillonDept contenant les informations d'un département
\param [in] ville chaine de charactère représentant le nom de la ville dans lequel ce trouve le département
\return void
*/
void save_line_iut(FILE *file, ListeDept ldept, char ville[31])
{
    fprintf(file,"%s\t\t%s\t%d%s\n",ville,ldept->nom,ldept->nbp,ldept->resp);
}
/**
\brief lit dans le fichier passer en paramètre les informations d'un département (une ligne du fichier iut)
\param [out] void void
\param [in] file pointeur de type FILE correspond au fichier iut
\param [in] ville chaine de charactère représentant le nom de la ville dans lequel ce trouve le département
\param [in] departement chaine de charactère représentant le nom du département
\param nbpers pointeur de type int représentant le nombre de place disponible dans le département
\param nom chaine de charactère représentant le nom du responsable du département
\return void
*/
void read_line_iut(char ville[31], char departement[31], int *nbpers, char nom[31], FILE *file)
{
    fscanf(file,"%s",ville);
    fscanf(file,"%s",departement);
    fscanf(file,"%d",nbpers);
    fgets(nom,31,file);
    nom[strlen(nom) -1] = '\0';
}
/**
\brief équivalent du script bash clear en shell
\param [out] void void
\param [in] void void
\return void
*/
void clear(void)
{
    printf("\e[1H;1\e[2J");
}
/**
\brief arrête le programe et affiche le message d'erreur passer en paramètre
\param [out] void void
\param [in] erreur chaine de charactère représentant un message d'erreur
\return void
*/
void stop(char *erreur)
{
    printf("%s",erreur);
    exit(1);
}
/**
\brief écrit dans le fichier candidat.don toutes les information contenu dans tc
\param [out] void void
\param [in] tc tableau de pointeur vers des structures de type Candidat
\param [in] tlc taille logique du tableau tc
\return void
*/
void save_candidat(Candidat *tc[], int tlc)
{
    FILE *file;
    file = fopen("./fichier/candidat.don","w");
    if (file == NULL) stop("erreur lors de l'écriture dans save_candidat(main.c)");
    fprintf(file,"%d\n",tlc);
    while(tlc>0)
    {
        tlc-=1;
        save_line_candidat(file,tc[tlc]->numc,tc[tlc]->nom,tc[tlc]->prenom,tc[tlc]->note,tc[tlc]->nbchoix);
        while(tc[tlc]->nbchoix>0)
        {
            save_line_demande(file,tc[tlc]->lchoix->ville,tc[tlc]->lchoix->dep,tc[tlc]->lchoix->desdp,tc[tlc]->lchoix->valcan);
            tc[tlc]->lchoix = supprimer_choix_en_tete(tc[tlc]->lchoix);
            tc[tlc]->nbchoix -= 1;
        }
    }
    fclose(file);
}
/**
\brief ecrit dans le fichier passer en paramètre les informations concernant un candidat eux aussi passer en paramètre
\param [out] void void
\param [in] file pointeur de type FILE correspond au fichier candidat.don
\param [in] numc numéro de candidat
\param [in] nom nom d'un candidat
\param [in] prenom prenom d'un candidat
\param [in] note tableau de float contenant 4 notes.
\param [in] nbchoix nombre de candidature du candidat
\return void
*/
void save_line_candidat(FILE *file, int numc, char nom[31], char prenom[31], float note[4], int nbchoix)
{
    fprintf(file,"%d\n",numc);
    fprintf(file,"%s\n",nom);
    fprintf(file,"%s\n",prenom);
    fprintf(file,"%.2f %.2f %.2f %.2f\n",note[0],note[1],note[2],note[3]);
    fprintf(file,"%d\n",nbchoix);
}
/**
\brief ecrit dans le fichier passer en paramètre les informations concernant une candidature eux aussi passer en paramètre
\param [out] void void
\param [in] file pointeur de type FILE correspond au fichier candidat.don
\param [in] ville chaine de charactère représentant le nom de la ville dans lequel ce trouve le département
\param [in] dep chaine de charactère représentant le nom du département
\param [in] desdp int représentant la décision du département
\param [in] valcan int représentant la validation du candidat 
\return void
*/
void save_line_demande(FILE *file, char ville[31], char dep[31], int desdp, int valcan)
{
    fprintf(file,"%s\n",ville);
    fprintf(file,"%s\n",dep);
    fprintf(file,"%d\n",desdp);
    fprintf(file,"%d\n",valcan);
}
/**
\brief sauvegarde les données contenu dans le tableau tiut dans le fichier iut en libérant la meimoir
\param [out] tc tableau de pointeur vers des structures de type Candidat 
\param [in] fichier chaine de charactère représentant le chemin relatif vers le fichier candidat.don
\param [in] tl taille logique du tableau tc
\return tc tableau de pointeur vers des structures de type Candidat
*/
Candidat** read_candidat(char fichier[],int *tl)
{
    float note[4];
    int numc, nbchoix, desdp, valcan, nbpers, pos, trouve;
    char nom[31], prenom[31], ville[31], dep[31];
    Candidat** tc;
    FILE *file;
    file = fopen(fichier,"r");
    if (file == NULL) stop("erreur lors de la lecture dans lire_iut(function.c)");
    fscanf(file,"%d",&nbpers);
    read_line_candidat(&numc,note,nom,prenom,&nbchoix,file);
    while(!feof(file))
    {
        if (*tl != 0) pos = recherche_candidat(tc,numc, *tl, &trouve);
        tc = ajouter_candidat(tc, tl, numc, note, nom, prenom,nbchoix,pos);
        while(nbchoix>0)
        {
            read_line_demande(ville, dep, &desdp, &valcan,file);
            tc[pos]->lchoix = ajouter_choix(tc[pos]->lchoix, ville, dep, desdp, valcan);
            nbchoix-=1;
        }
        read_line_candidat(&numc,note,nom,prenom,&nbchoix,file);
    }
    if ( *tl != nbpers) stop("erreur, le nombre de candidat ne correspond pas");
    return tc;
}
/**
\brief lit dans le fichier passer en paramètre les informations concernant une candidature
\param [out] void void
\param [in] file pointeur de type FILE correspond au fichier candidat.don
\param [in] ville chaine de charactère représentant le nom de la ville dans lequel ce trouve le département
\param [in] dep chaine de charactère représentant le nom du département
\param [in] desdp int représentant la décision du département
\param [in] valcan int représentant la validation du candidat 
\return void
*/
void read_line_demande(char ville[31], char dep[31], int *desdp, int *valcan, FILE *file)
{
    fscanf(file,"%s",ville);
    fscanf(file,"%s",dep);
    fscanf(file,"%d",desdp);
    fscanf(file,"%d",valcan);
}
/**
\brief lit dans le fichier passer en paramètre les informations concernant un candidat
\param [out] void void
\param [in] file pointeur de type FILE correspond au fichier candidat.don
\param [in] numc numéro de candidat
\param [in] nom nom d'un candidat
\param [in] prenom prenom d'un candidat
\param [in] note tableau de float contenant 4 notes.
\param [in] nbchoix nombre de candidature du candidat
\return void
*/
void read_line_candidat(int *numc, float note[4], char nom[31], char prenom[31], int *nbchoix, FILE *file)
{
    fscanf(file,"%d",numc);
    fscanf(file,"%s",nom);
    fscanf(file,"%s",prenom);
    fscanf(file,"%f",&note[0]);
    fscanf(file,"%f",&note[1]);
    fscanf(file,"%f",&note[2]);
    fscanf(file,"%f",&note[3]);
    fscanf(file,"%d",nbchoix);
}
/**
\brief permet de charger en mémoire centrale dans tiut certaine information en commun et contenu dans tc
\param [out] void void
\param [in] tc tableau de pointeur vers des structures de type Candidat
\param [in] tlc taille logique du tableau tc
\param [in] tiut tableau de pointeur vaire des structures VilleIUT possédant une liste chainé
\param [in] tl taille logique du tableau tiut
\return void
*/
void charger_demande(VilleIUT *tiut[],int tl, Candidat *tc[], int tlc)
{
    int pos, trouve;
    maillonChoix *x;
    for (int i=0; i<tlc; i++)
    {
        x = tc[i]->lchoix;
        while( x != NULL)
        {
            pos = recherche_ville(tiut,x->ville,&trouve,tl);
            if (trouve == 0) stop("\nun candidat à postuler dans une ville qui n'existe pas\n");
            ajouter_demande_ldept(tiut[pos]->ldept,x->dep, tc[i]->nom, tc[i]->numc, x->desdp, x->valcan);
            x = x->suiv;
        }
    }
}