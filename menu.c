/**
*\file menu.c
*\brief fichier contenant la function global (fonction qui lance toutes les autres) et les fonctions en rapport avec le menu du programme
*author Ludovic CASTIGLIA
*/
#include"struct.h"
#include"main.h"
#include"menu.h"
/**
\brief fonction globale qui lance toutes les autres
\param [out] void void
\param [in] void void
\return void
*/
void global(void)
{
    int tl = 0,tlc = 0;
    Candidat** tc = read_candidat("./fichier/candidat.don",&tlc);
    printf("chargement candidat effectué\n");
    VilleIUT **tiut = lire_iut("./fichier/iut", &tl);
    printf("chargement iut effectué\n");
    charger_demande(tiut,tl,tc,tlc);
    printf("chargement demande effectué\n");
    int typeUtilisateur=0, sousMenu=0, sorti=0;
    while (1)
    {
        if ( typeUtilisateur == 0 ) {typeUtilisateur = menu_utilisateur(); sousMenu=0;}
        if ( typeUtilisateur == 1 )
        {
            if ( sousMenu == 0 || re_menu() == 1 ) sousMenu = sous_menu_utilisateur();
            if ( sousMenu == 1) affiche_villes(tiut, tl);
            if ( sousMenu == 2) affiche_departement(tiut, tl);
            if ( sousMenu == 3) affiche_nb_place(tiut, tl);
            if ( sousMenu == 4) affiche_iut(tiut, tl);
            if ( sousMenu == 5) ajouter_un_voeux(tiut,tl,tc,tlc);
            if ( sousMenu == 6) supprimer_un_voeux(tiut,tl,tc,tlc);
            if ( sousMenu == 7) affiche_les_voeux(tc, tlc);
            if ( sousMenu == 8) typeUtilisateur = 0;
        }
        if ( typeUtilisateur == 2 )
        {
            if ( sousMenu == 0 || re_menu() == 1 ) sousMenu = sous_menu_admin();
            if ( sousMenu == 1) cree_departement(tiut, tl);
            if ( sousMenu == 2) suppr_departement(tiut, tl);
            if ( sousMenu == 4) modif_nom_departement(tiut, tl);
            if ( sousMenu == 8) typeUtilisateur = 0;
        }
        if ( typeUtilisateur == 3 )
        {
            if ( sousMenu == 0 || re_menu() == 1 ) sousMenu = sous_menu_responsable();
            if ( sousMenu == 1) affiche_tout_candidat(tc, tlc);
            if ( sousMenu == 2) affiche_un_candidat(tc, tlc);
            if ( sousMenu == 3) affiche_candidat_departement(tiut, tl);
            if ( sousMenu == 8) typeUtilisateur = 0;
        }
        if ( typeUtilisateur == 9 || sousMenu == 9 ) break;
    }
    save_iut(tiut, tl);
    save_candidat(tc,tlc);
    merci();
}
/**
\brief fonction qui récupère et vérifi la saisit du menu utilisateur
\param [out] int saisit de l'uttilisateur
\param [in] void void
\return la saisit de l'uttilisateur
*/
int menu_utilisateur(void)
{
    int res;
    menu_utilisateur_affiche();
    scanf("%d",&res);
    while( res!=9 && res!=1 && res!=2 && res!=3)
    {
        menu_utilisateur_affiche();
        printf("\nerreur, la valeur saisie n'est pas correcte, veuiller resaisir\n");
        scanf("%d",&res);
    }
    return res;
}
/**
\brief affiche le menu uttilisateur
\param [out] void void
\param [in] void void
\return void
*/
void menu_utilisateur_affiche(void)
{
    clear();
    printf("\n\t\t####################################################################\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##                     qui êtes vous ?                            ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##                1. étudiant                                     ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##                2. Un administrateur                            ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##                3. responsable                                  ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##                9. sortir de ce programe                        ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t####################################################################\n");
}
/**
\brief fonction qui récupère et vérifi la saisit du menu étudiant
\param [out] int saisit de l'uttilisateur
\param [in] void void
\return la saisit de l'uttilisateur
*/
int sous_menu_utilisateur(void)
{
    int res;
    sous_menu_utilisateur_affiche();
    scanf("%d",&res);
    while( res<1 || res>9 )
    {
        sous_menu_utilisateur_affiche();
        printf("\nerreur, la valeur saisie n'est pas correcte, veuiller resaisir\n");
        scanf("%d",&res);
    }
    return res;
}
/**
\brief affiche le menu étudient
\param [out] void void
\param [in] void void
\return void
*/
void sous_menu_utilisateur_affiche(void)
{
    clear();
    printf("\n\t\t####################################################################\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##                que voulez vous faire ?                         ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          1. afficher les villes                                ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          2. afficher les département d'un IUT                  ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          3. afficher le nombre de place                        ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          4. afficher les IUT avec un département particulier   ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          5. ajouter un voeu                                    ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          6. supprimer un voeu                                  ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          7. afficher tout nos voeux                            ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          8. revenir au menu précédent                          ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          9. sortir de ce programe                              ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t####################################################################\n");
}
/**
\brief fonction qui récupère et vérifi la saisit du menu admin
\param [out] int saisit de l'uttilisateur
\param [in] void void
\return la saisit de l'uttilisateur
*/
int sous_menu_admin(void)
{
    int res;
    sous_menu_admin_affiche();
    scanf("%d",&res);
    while( res!=8 && res!=9 && ( res<1 || res>4 ) )
    {
        sous_menu_admin_affiche();
        printf("\nerreur, la valeur saisie n'est pas correcte, veuiller resaisir\n");
        scanf("%d",&res);
    }
    return res;
}
/**
\brief affiche le menu admin
\param [out] void void
\param [in] void void
\return void void
*/
void sous_menu_admin_affiche(void)
{
    clear();
    printf("\n\t\t####################################################################\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##                que voulez vous faire ?                         ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          1. créer un département dans un IUT                   ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          2. supprimer un département d’un IUT                  ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          3. lancer/stoper la phase de candidature              ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          4. modifier le nom du responsable d’un département    ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          8. revenir au menu précédent                          ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          9. sortir de ce programe                              ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t####################################################################\n");
}
/**
\brief fonction qui récupère et vérifi la saisit du menu responsable
\param [out] int saisit de l'uttilisateur
\param [in] void void
\return la saisit de l'uttilisateur
*/
int sous_menu_responsable(void)
{
    int res;
    sous_menu_responsable_affiche();
    scanf("%d",&res);
    while( res!=8 && res!=9 && ( res<1 || res>3 ) )
    {
        sous_menu_responsable_affiche();
        printf("\nerreur, la valeur saisie n'est pas correcte, veuiller resaisir\n");
        scanf("%d",&res);
    }
    return res;
}
/**
\brief affiche le menu responsable
\param [out] void void
\param [in] void void
\return void
*/
void sous_menu_responsable_affiche(void)
{
    clear();
    printf("\n\t\t####################################################################\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##                que voulez vous faire ?                         ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          1. afficher tout les candidats                        ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          2. afficher un candidat                               ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          3. afficher les candidats par département             ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          8. revenir au menu précédent                          ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##          9. sortir de ce programe                              ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t####################################################################\n");
}
/**
\brief affiche le message de remerciment à la fin de l'execution du programe
\param [out] void void
\param [in] void void
\return void void
*/
void merci(void)
{
    clear();
    printf("\n\t\t####################################################################\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##             Merci d'avoir uttilisé ce logiciel                 ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##                         A bientôt                              ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t##     signé: Ludovic CASTIGLIA                                   ##\n");
    printf("\t\t##                                                                ##\n");
    printf("\t\t####################################################################\n");
}
/**
\brief demande à l'uttilisateur si il veux revenir au menu précédent après l'execution d'une fonction (tout en contrôlent la saisie)
\param [out] int saisit de l'uttilisateur
\param [in] void void
\return la saisit de l'uttilisateur
*/
int re_menu(void)
{
    int res;
    printf("\n\nvoulez revenir au menu ? (0/1): ");
    scanf("%d",&res);
    while(res!=0 && res!=1)
    {
        printf("la saisie n'est pas conforme.\n");
        printf("\n\nvoulez revenir au menu ? (0/1): ");
        scanf("%d",&res);
    }
    return res;
}