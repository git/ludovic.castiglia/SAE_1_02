var searchData=
[
  ['decalage_5fa_5fdroite_27',['decalage_a_droite',['../struct_8c.html#afe6b3de3262b1babec874f4f0e664170',1,'decalage_a_droite(VilleIUT **tiut, int pos, int tl):&#160;struct.c'],['../struct_8h.html#afe6b3de3262b1babec874f4f0e664170',1,'decalage_a_droite(VilleIUT **tiut, int pos, int tl):&#160;struct.c']]],
  ['decalage_5fa_5fdroite_5fcandidat_28',['decalage_a_droite_candidat',['../struct_8c.html#abb1fea78297796c9981546ceefa18946',1,'decalage_a_droite_candidat(Candidat **tc, int pos, int tl):&#160;struct.c'],['../struct_8h.html#abb1fea78297796c9981546ceefa18946',1,'decalage_a_droite_candidat(Candidat **tc, int pos, int tl):&#160;struct.c']]],
  ['demande_5fexiste_29',['demande_existe',['../struct_8c.html#a7af7e026cfd6af8e6b7d530be2e8a4f9',1,'demande_existe(ListeChoix l, char ville[31], char dep[31]):&#160;struct.c'],['../struct_8h.html#a7af7e026cfd6af8e6b7d530be2e8a4f9',1,'demande_existe(ListeChoix l, char ville[31], char dep[31]):&#160;struct.c']]],
  ['departement_5fin_5fville_30',['departement_in_ville',['../struct_8c.html#a420832377f6d8cfb68922f896fec1b0c',1,'departement_in_ville(maillonDept *m, char dep[31]):&#160;struct.c'],['../struct_8h.html#a420832377f6d8cfb68922f896fec1b0c',1,'departement_in_ville(maillonDept *m, char dep[31]):&#160;struct.c']]]
];
