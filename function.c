/**
*\file function.c
*\brief ensemble des fonctions applelé dans menu.c et qui uttilise les functions de struct.c.
*author Ludovic CASTIGLIA
*/


#include"struct.h"
#include"main.h"
#include"menu.h"

/**
\brief affiche toutes les villes possédant un IUT
\param [out] void void
\param [in] tiut tableau de pointeur vaire des structures VilleIUT possédant une liste chainé
\param [in] tl taille logique du tableau tiut
\return void
*/
void affiche_villes(VilleIUT *tiut[], int tl)
{
    clear();
    printf("############################\n");
    printf("#\t\tnom de la ville\n");
    for (int i=0; i<tl; i++)
        printf("#\t%s\n",tiut[i]->ville);
}
/**
\brief affiche toute les département dans un IUT ansi que le nombre de place et le nom du responsable
\param [out] void void
\param [in] tiut tableau de pointeur vaire des structures VilleIUT possédant une liste chainé
\param [in] tl taille logique du tableau tiut
\return void
*/
void affiche_departement(VilleIUT *tiut[], int tl)
{
    char ville[31];
    int trouve=0,pos;
    clear();
    printf("\n\nquelle ville voulez vous affichez: ");
    scanf("%*c");
    fgets(ville,31,stdin);
    ville[strlen(ville) -1] = '\0';
    pos = recherche_ville(tiut,ville,&trouve, tl);
    if (!trouve)
    {
        printf("désolé, cette ville n'existe pas");
        return;
    }
    printf("\n\ndépartement\tplace\tnom du responsable\n\n");
    affiche_ListeDept(tiut[pos]->ldept);
}
/**
\brief affiche le nombre de place dans un département particulier
\param [out] void void
\param [in] tiut tableau de pointeur vaire des structures VilleIUT possédant une liste chainé
\param [in] tl taille logique du tableau tiut
\return void
*/
void affiche_nb_place(VilleIUT *tiut[], int tl)
{
    char ville[31], dep[31];
    int trouve=0,pos,nbp;
    clear();
    printf("\n\ndans quelle ville est le département: ");
    scanf("%*c");
    fgets(ville,31,stdin);
    ville[strlen(ville) -1] = '\0';
    pos = recherche_ville(tiut,ville,&trouve, tl);
    if (!trouve)
    {
        printf("désolé, cette ville n'existe pas");
        return;
    }
    printf("\n\nde quelle département sagit t'il: ");
    fgets(dep,31,stdin);
    dep[strlen(dep) -1] = '\0';
    nbp = nb_place_departement(tiut[pos]->ldept,dep);
    if (nbp == -1)
    {
        printf("\n\n%s ne comporte pas de département %s\n",ville,dep);
        return;
    }
    printf("\n\nle département %s de %s à %d place disponible\n",dep,ville,nbp);
}
/**
\brief affiche tout les IUT qui possède un certain département
\param [out] void void
\param [in] tiut tableau de pointeur vaire des structures VilleIUT possédant une liste chainé
\param [in] tl taille logique du tableau tiut
\return void
*/
void affiche_iut(VilleIUT *tiut[], int tl)
{
    int trouve=0;
    char dep[31];
    clear();
    printf("\n\nquelle département chercher vous?: ");
    scanf("%*c");
    fgets(dep,31,stdin);
    dep[strlen(dep) -1] = '\0';
    while (tl > 0)
    {
        tl-=1;
        if (departement_in_ville(tiut[tl]->ldept, dep) == 1)
        {
            if (trouve == 0)
            {
                printf("\n\nil y a un département %s à:\n",dep);
                trouve=1;
            }
            printf("%s\n",tiut[tl]->ville);
        }
    }
    if (trouve==0) printf("aucune ville ne possède ce département.\n");
}
/**
\brief permet de crée un nouveau département dans un certain IUT
\param [out] void void
\param [in] tiut tableau de pointeur vaire des structures VilleIUT possédant une liste chainé
\param [in] tl taille logique du tableau tiut
\return void
*/
void cree_departement(VilleIUT *tiut[], int tl)
{
    char ville[31], dep[31], nom[31];
    int trouve=0,pos,nbp;
    clear();
    printf("\n\ndans quelle ville est le département: ");
    scanf("%*c");
    fgets(ville,31,stdin);
    ville[strlen(ville) -1] = '\0';
    pos = recherche_ville(tiut,ville,&trouve, tl);
    if (!trouve)
    {
        printf("désolé, cette ville n'existe pas");
        return;
    }
    printf("\n\nquelle département voulez vous ajouter: ");
    fgets(dep,31,stdin);
    dep[strlen(dep) -1] = '\0';
    if (departement_in_ville(tiut[pos]->ldept, dep) == 1)
    {
        printf("désolé, ce département existe déjà");
        return;
    }
    printf("\n\ncombien de place sont disponible: ");
    scanf("%d",&nbp);
    printf("\n\nquelle est le nom du responsable du département: ");
    scanf("%*c");
    fgets(nom,31,stdin);
    nom[strlen(nom)-1] = '\0';
    tiut[pos]->ldept = ajouter_departement(tiut[pos]->ldept,dep,nbp,nom);
}
/**
\brief permet de supprimer un derpartement d'un certain IUT
\param [out] void void
\param [in] tiut tableau de pointeur vaire des structures VilleIUT possédant une liste chainé
\param [in] tl taille logique du tableau tiut
\return void
*/
void suppr_departement(VilleIUT *tiut[], int tl)
{
    char ville[31], dep[31];
    int trouve=0,pos;
    clear();
    printf("\n\ndans quelle ville est le département: ");
    scanf("%*c");
    fgets(ville,31,stdin);
    ville[strlen(ville) -1] = '\0';
    pos = recherche_ville(tiut,ville,&trouve, tl);
    if (!trouve)
    {
        printf("désolé, cette ville n'existe pas");
        return;
    }
    printf("\n\nquelle département voulez vous supprimer: ");
    fgets(dep,31,stdin);
    dep[strlen(dep) -1] = '\0';
    if (departement_in_ville(tiut[pos]->ldept, dep) == 0)
    {
        printf("désolé, ce département n'existe pas dans cette ville");
        return;
    }
    tiut[pos]->ldept = supprimer_departement(tiut[pos]->ldept, dep);
}
/**
\brief permet de modifier le nom du responsable d'un certain département
\param [out] void void
\param [in] tiut tableau de pointeur vaire des structures VilleIUT possédant une liste chainé
\param [in] tl taille logique du tableau tiut
\return void
*/
void modif_nom_departement(VilleIUT *tiut[], int tl)
{
    char ville[31], dep[31], nom[31];
    int trouve=0,pos,nbp=0;
    clear();
    printf("\n\ndans quelle ville est le département: ");
    scanf("%*c");
    fgets(ville,31,stdin);
    ville[strlen(ville) -1] = '\0';
    pos = recherche_ville(tiut,ville,&trouve, tl);
    if (!trouve)
    {
        printf("désolé, cette ville n'existe pas");
        return;
    }
    printf("\n\nquelle département voulez vous modifier: ");
    fgets(dep,31,stdin);
    dep[strlen(dep) -1] = '\0';
    if (departement_in_ville(tiut[pos]->ldept, dep) == 0)
    {
        printf("désolé, ce département n'existe pas");
        return;
    }
    printf("\n\nquelle est le nom du responsable du département: ");
    fgets(nom,31,stdin);
    nom[strlen(nom)-1] = '\0';
    tiut[pos]->ldept = ajouter_departement(tiut[pos]->ldept,dep,nbp,nom);
}
/**
\brief affiche toutes les informations d'un certain candidat (nom,prenom,note,nombre de voeux et les voeux en question)
\param [out] void void
\param [in] tc tableau de pointeur vaire des structures Candidat possédant une liste chainé
\param [in] tlc taille logique du tableau tc
\return void
*/
void affiche_un_candidat(Candidat** tc, int tlc)
{
    int pos, trouve, numc;
    clear();
    printf("\n\nquelle est le numéro de candidat ");
    scanf("%d",&numc);
    while (numc <= 0)
    {
        clear();
        printf("\nnuméro de candidat non valide");
        printf("\nquelle est le numéro de candidat ");
        scanf("%d",&numc);
    }
    pos = recherche_candidat(tc,numc,tlc,&trouve);
    if (trouve == 0) 
    {
        printf("\nce candidat n'existe pas\n");
        return;
    }
    printf("\n\nn°candidat\tnom\tprenom\t\t\tnote\t\t\t\t\tnombre de veux\n");
    affiche_candidat(*tc[pos]);
    printf("\n\nville\t\tdepartement\n");
    affiche_ListeChoix(tc[pos]->lchoix);
}
/**
\brief affiche tout les candidats ayant postulé dans un département
\param [out] void void
\param [in] tiut tableau de pointeur vaire des structures Candidat possédant une liste chainé
\param [in] tl taille logique du tableau tiut
\return void
*/
void affiche_candidat_departement(VilleIUT *tiut[], int tl)
{
    char ville[31], dep[31];
    int trouve=0,pos,nbp;
    clear();
    printf("\n\ndans quelle ville est le département: ");
    scanf("%*c");
    fgets(ville,31,stdin);
    ville[strlen(ville) -1] = '\0';
    pos = recherche_ville(tiut,ville,&trouve, tl);
    if (!trouve)
    {
        printf("désolé, cette ville n'existe pas");
        return;
    }
    printf("\n\nde quelle département sagit t'il: ");
    fgets(dep,31,stdin);
    dep[strlen(dep) -1] = '\0';
    affiche_candidat_departement_ldept(tiut[pos]->ldept,dep);
}
/**
\brief affiche tout les voeux d'un certain candidat
\param [out] void void
\param [in] tc tableau de pointeur vaire des structures Candidat possédant une liste chainé
\param [in] tlc taille logique du tableau tc
\return void
*/
void affiche_les_voeux(Candidat** tc, int tlc)
{
    int pos, trouve, numc;
    clear();
    printf("\n\nquelle est votre numéro de candidat ");
    scanf("%d",&numc);
    while (numc <= 0)
    {
        clear();
        printf("\nnuméro de candidat non valide");
        printf("\nquelle est votre numéro de candidat ");
        scanf("%d",&numc);
    }
    pos = recherche_candidat(tc,numc,tlc,&trouve);
    if (trouve == 0) 
    {
        printf("\nce numéro de candidat n'est pas attribué\n");
        return;
    }
    printf("\n\nville\t\tdepartement\n");
    affiche_ListeChoix(tc[pos]->lchoix);
}
/**
\brief permet à un candidat de postuler dans un département
\param [out] void void
\param [in] tiut tableau de pointeur vaire des structures Candidat possédant une liste chainé
\param [in] tl taille logique du tableau tiut
\param [in] tc tableau de pointeur vaire des structures Candidat possédant une liste chainé
\param [in] tlc taille logique du tableau tc
\return void
*/
void ajouter_un_voeux(VilleIUT *tiut[], int tl, Candidat** tc, int tlc)
{
    int posCan, posVil, trouve, numc;
    char dep[31],ville[31];
    clear();
    printf("\n\nquelle est votre numéro de candidat ");
    scanf("%d",&numc);
    while (numc <= 0)
    {
        clear();
        printf("\nnuméro de candidat non valide");
        printf("\nquelle est votre numéro de candidat ");
        scanf("%d",&numc);
    }
    posCan = recherche_candidat(tc,numc,tlc,&trouve);
    if (trouve == 0) 
    {
        printf("\nce numéro de candidat n'est pas attribué\n");
        return;
    }
    printf("\n\ndans quelle ville est le département: ");
    scanf("%*c");
    fgets(ville,31,stdin);
    ville[strlen(ville) -1] = '\0';
    posVil = recherche_ville(tiut,ville,&trouve, tl);
    if (!trouve)
    {
        printf("désolé, cette ville n'existe pas");
        return;
    }
    printf("\n\ndans quelle département voulez vous candidater: ");
    fgets(dep,31,stdin);
    dep[strlen(dep) -1] = '\0';
    if (departement_in_ville(tiut[posVil]->ldept, dep) == 0)
    {
        printf("désolé, ce département n'existe pas dans cette ville");
        return;
    }
    if (demande_existe(tc[posCan]->lchoix,ville,dep)) 
    {
        printf("vous avez déjà postulé pour ce département");
        return;
    }
    ajouter_demande_ldept(tiut[posVil]->ldept,dep, tc[posCan]->nom, tc[posCan]->numc, 0, 0);
    tc[posCan]->lchoix = ajouter_choix(tc[posCan]->lchoix, ville, dep, 0, 0);
    tc[posCan]->nbchoix += 1;
}
/**
\brief permet à un candidat d'annuler ça candidature dans un département
\param [out] void void
\param [in] tiut tableau de pointeur vaire des structures Candidat possédant une liste chainé
\param [in] tl taille logique du tableau tiut
\param [in] tc tableau de pointeur vaire des structures Candidat possédant une liste chainé
\param [in] tlc taille logique du tableau tc
\return void
*/
void supprimer_un_voeux(VilleIUT *tiut[], int tl, Candidat** tc, int tlc)
{
    int posCan, posVil, trouve, numc;
    char dep[31],ville[31];
    clear();
    printf("\n\nquelle est votre numéro de candidat ");
    scanf("%d",&numc);
    while (numc <= 0)
    {
        clear();
        printf("\nnuméro de candidat non valide");
        printf("\nquelle est votre numéro de candidat ");
        scanf("%d",&numc);
    }
    posCan = recherche_candidat(tc,numc,tlc,&trouve);
    if (trouve == 0)
    {
        printf("\nce numéro de candidat n'est pas attribué\n");
        return;
    }
    printf("\n\ndans quelle ville est le département: ");
    scanf("%*c");
    fgets(ville,31,stdin);
    ville[strlen(ville) -1] = '\0';
    posVil = recherche_ville(tiut,ville,&trouve, tl);
    if (!trouve)
    {
        printf("désolé, cette ville n'existe pas");
        return;
    }
    printf("\n\ndans quelle département voulez vous supprimer votre candidature: ");
    fgets(dep,31,stdin);
    dep[strlen(dep) -1] = '\0';
    if (departement_in_ville(tiut[posVil]->ldept, dep) == 0)
    {
        printf("désolé, ce département n'existe pas dans cette ville");
        return;
    }
    if (!demande_existe(tc[posCan]->lchoix,ville,dep))
    {
        printf("vous n'avez jamais postulé pour ce département");
        return;
    }
    supprimer_demande_ldept(tiut[posVil]->ldept,dep, tc[posCan]->nom);
    tc[posCan]->lchoix = supprimer_choix(tc[posCan]->lchoix, ville, dep);
    tc[posCan]->nbchoix -= 1;
}