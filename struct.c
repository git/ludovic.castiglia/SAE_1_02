/**
*\file struct.c
*\brief fichier contenant toutes les fonctions qui intéragissent avec les structures et qui sont appelé dans fonction.c
*author Ludovic CASTIGLIA
*/
#include"struct.h"
#include"main.h"
#include"menu.h"
/**
\brief initialise une liste chainé de département vide
\param [out] ListeDept liste chainé de département
\param [in] void void
\return ListeDept liste chainé de département
*/
ListeDept fil_nouv_ldept(void)
{
    return NULL;
}
/**
\brief initialise une liste chainé de candidature vide
\param [out] ListeDept liste chainé de candidature
\param [in] void void
\return ListeDept liste chainé de candidature
*/
ListeChoix fil_nouv_lchoix(void)
{
    return NULL;
}
/**
\brief ajoute un département dans un liste chainé de département ldept
\param [out] ListeDept liste chainé de département
\param [in] ldept liste chainé de département
\param [in] nom nom du département
\param [in] nbp nombre de place disponible dans ce département
\parma [in] resp nom et prénom du responsable du département
\return ListeDept liste chainé de département
*/
ListeDept ajouter_departement(ListeDept ldept,char nom[31], int nbp, char resp[31])
{
    if (ldept == NULL) return ajouter_departement_en_tete(ldept,nom,nbp,resp);
    if (strcmp(ldept->nom,nom) < 0)
    {
        ldept = ajouter_departement_en_tete(ldept,nom,nbp,resp);
        return ldept;
    }
    if (strcmp(ldept->nom,nom) == 0) 
    {
        strcpy(ldept->resp,resp);
        return ldept;
    }
    ldept->suiv = ajouter_departement(ldept->suiv,nom,nbp,resp);
    return ldept;
}
/**
\brief suprime un département dans un liste chainé de département ldept
\param [out] ListeDept liste chainé de département
\param [in] ldept liste chainé de département
\param [in] nom nom du département
\return ListeDept liste chainé de département
*/
ListeDept supprimer_departement(ListeDept ldept,char nom[31])
{
    if (ldept == NULL) return supprimer_departement(ldept,nom);
    if (strcmp(ldept->nom,nom) == 0)
    {
        ldept = supprimer_departement_en_tete(ldept);
        return ldept;
    }
    ldept->suiv = supprimer_departement(ldept->suiv,nom);
    return ldept;
}
/**
\brief retourn le nombre de place disponible dans un département
\param [out] int nombre de place dans le département
\param [in] dep nom du département
\param [in] m liste chainé de département
\return int nombre de place dand le déparement en question
*/
int nb_place_departement(maillonDept *m, char dep[31])
{
    if (m == NULL) return -1;
    if (strcmp(m->nom,dep) == 0) return m->nbp;
    return nb_place_departement(m->suiv,dep);
}
/**
\brief permet de savoir si un département existe dans un ville
\param [out] int 0 si le département n'existe pas et 1 si le département existe
\param [in] dep nom du département
\param [in] m liste chainé de département
\return int 0 si le département n'existe pas et 1 si le département existe
*/
int departement_in_ville(maillonDept *m, char dep[31])
{
    if (m == NULL) return 0; 
    if (strcmp(m->nom,dep) == 0) return 1;
    return departement_in_ville(m->suiv,dep);
}
/**
\brief ajoute un département en tête de liste chainé
\param [out] ListeDept liste chainé de département
\param [in] ldept liste chainé de département
\param [in] nom nom du département
\param [in] nbp nombre de place disponible dans le département
\param [in] resp nom du responsable du département
\return ListeDept liste chainé de département
*/
ListeDept ajouter_departement_en_tete(ListeDept ldept,char nom[31], int nbp, char resp[31])
{
    maillonDept *x = (maillonDept *) malloc(sizeof(maillonDept));
    if (x == NULL) stop("problème de malloc dans ajouter_departement (struct.c)");
    strcpy(x->nom,nom);
    strcpy(x->resp,resp);
    x->nbp = nbp;
    x->suiv = ldept;
    x->dem = NULL;
    return x;
}
/**
\brief supprime un département en tête de liste chainé
\param [out] ListeDept liste chainé de département
\param [in] ldept liste chainé de département
\return ListeDept liste chainé de département
*/
ListeDept supprimer_departement_en_tete(ListeDept ldept)
{
    maillonDept *x;
    x=ldept->suiv;
    free(ldept);
    return x;
}
/**
\brief ajoute une ville dans le tableau tiut
\param [out] tiut tableau de pointeur de type VilleIUT
\param [in] tiut tableau de pointeur de type VilleIUT
\param [in] tl taille logique du tableau tiut
\param [in] nom nom de la ville
\param [in] pos position d'incertion de la ville dans le tableau tiut
\return VilleIUT** tableau de pointeur de type VilleIUT
*/
VilleIUT** ajouter_ville(VilleIUT *tiut[], int *tl, char nom[31],int pos)
{
    *tl += 1;
    VilleIUT **tiut2;
    tiut2 = (VilleIUT **) realloc(tiut,sizeof(VilleIUT *) * (*tl + 2));
    if (tiut2 == NULL) stop("erreur de realloc dans ajouter_ville(struct.c)");
    tiut = tiut2;
    tiut = decalage_a_droite(tiut,pos,*tl);
    tiut[pos] = (VilleIUT *) malloc(sizeof(VilleIUT));
    if (tiut[pos] == NULL) stop("erreur de malloc dans ajouter_ville(struct.c)");
	strcpy(tiut[pos]->ville,nom);
    tiut[pos]->ldept = fil_nouv_ldept();
    return tiut;
}
/**
\brief décalle toutes les villes dans le tableau tiut à partir de la position pos passé en paramètre
\param [out] tiut tableau de pointeur de type VilleIUT
\param [in] tiut tableau de pointeur de type VilleIUT
\param [in] pos position à partir de laquelle toutes les villes doivent être décallé
\param [in] tl taille logique du tableau tiut
\return VilleIUT** tableau de pointeur de type VilleIUT
*/
VilleIUT** decalage_a_droite(VilleIUT** tiut, int pos, int tl)
{
    int nbe = tl - pos;
    while(nbe>=0)
    {
        tiut[nbe+1+pos] = tiut[nbe+pos];
        nbe -= 1;
    }
    return tiut;
}
/**
\brief décalle toutes les candidat dans le tableau tc à partir de la position pos passé en paramètre
\param [out] ListeDept liste chainé de candidat
\param [in] tc tableau de pointeur de type Candidat
\param [in] pos position à partir de laquelle toutes les villes doivent être décallé
\param [in] tl taille logique du tableau tc
\return Candidat** tableau de pointeur de type Candidat
*/
Candidat** decalage_a_droite_candidat(Candidat** tc, int pos, int tl)
{
    int nbe = tl - pos;
    while(nbe>=0)
    {
        tc[nbe+1+pos] = tc[nbe+pos];
        nbe -= 1;
    }
    return tc;
}
/**
\brief recherche une ville dans tiut et retourne ça possition (réel ou d'insertion) en plus de présisez si elle existe déjà ou non.
\param [out] int position réel ou d'insertion de la ville dans tiut
\param [in] tiut tableau de pointeur de type VilleIUT
\param [in] trouve pointeur de type int qui permet de déduire l'existence ou non de la ville dans tiut après l'execution de cette fonction.
\param [in] tl taille logique du tableau tiut
\return int position réel ou d'insertion de la ville dans tiut
*/
int recherche_ville(VilleIUT **tiut,char ville[31],int *trouve,int tl)
{
    int inf = 0, sup = tl - 1, m;
    while (sup >= inf){
        m = (sup+inf)/2;
        if (strcmp(tiut[m]->ville,ville) == 0) {*trouve = 1; return m;}
        if (strcmp(tiut[m]->ville,ville) < 0) inf = m + 1;
        else sup = m - 1;
    }
    *trouve = 0;
    return inf;
}
/**
\brief affiche d'une manière récursive tout les département
\param [out] void void
\param [in] m liste chainé de type maillonDept
\return void void
*/
void affiche_ListeDept(maillonDept *m)
{
    if (m == NULL) return;
    printf("%s\t%d\t%s\n",m->nom,m->nbp,m->resp);
    affiche_ListeDept(m->suiv);
}
/**
\brief affiche d'une manière récursive toutes les demandes
\param [out] void void
\param [in] m liste chainé de type demande
\return void void
*/
void affiche_demande(demande *m)
{
    if (m == NULL) return;
    printf("%s\t%d\t%d\t%d\n",m->nom,m->numc,m->desdp,m->valcan);
    affiche_demande(m->prec);
}
/**
\brief affiche d'une manière récursive tout les choix
\param [out] void void
\param [in] m liste chainé de type maillonChoix
\return void void
*/
void affiche_ListeChoix(ListeChoix m)
{
    if (m == NULL) return;
    printf("%s\t%s\t%d\t%d\n",m->ville,m->dep,m->desdp,m->valcan);
    affiche_ListeChoix(m->suiv);
}
/**
\brief affiche toutes les informations d'un candidat (numéro de candidat,nom,prenom,note,nombre de voeux)
\param [out] void void
\param [in] c structure de type candidat
\return void void
*/
void affiche_candidat(Candidat c)
{
    printf("%d\t\t%s\t%s\t\t%.2f\t%.2f\t%.2f\t%.2f\t\t%d\n",c.numc,c.nom,c.prenom,c.note[0],c.note[1],c.note[2],c.note[3],c.nbchoix);
}
/**
\brief ajoute un candidat dans le tableau tc
\param [out] tc tableau de pointeur sur des Candidat
\param [in] tc tableau de pointeur sur des Candidat
\param [in] tl taille logique du tableau tc
\param [in] numc numéro de candidat
\param [in] note tableau de 4 float correspond au note du candidat
\param [in] nom nom du candidat
\param [in] prenom prénom du candidat
\param [in] nbchoix nombre de voeux du candidat
\param [in] pos position à lequel il faut insérer le candidat dans tc
\return Candidat** tableau de pointeur sur des Candidat
*/
Candidat** ajouter_candidat(Candidat *tc[], int *tl, int numc, float note[4], char nom[31], char prenom[31], int nbchoix,int pos)
{
    *tl += 1;
    Candidat **tc2;
    tc2 = (Candidat **) realloc(tc,sizeof(Candidat *) * (*tl + 2));
    if (tc2 == NULL) stop("erreur de realloc dans ajouter_Candidat(struct.c)");
    tc = tc2;
    tc = decalage_a_droite_candidat(tc,pos,*tl);
    tc[pos] = (Candidat *) malloc(sizeof(Candidat));
    if (tc[pos] == NULL) stop("erreur de malloc dans ajouter_Candidat(struct.c)");
	tc[pos]->numc = numc;
    strcpy(tc[pos]->nom,nom);
    strcpy(tc[pos]->prenom,prenom);
    tc[pos]->note[0] = note[0];
    tc[pos]->note[1] = note[1];
    tc[pos]->note[2] = note[2];
    tc[pos]->note[3] = note[3];
    tc[pos]->nbchoix = nbchoix;
    tc[pos]->lchoix = fil_nouv_lchoix();
    return tc;
}
/**
\brief retourne la position réel ou d'insertion d'un candidat dans tc tout en présisant si ce candidat existe déjà
\param [out] pos position réel ou d'insertion du candidat dans le tableau tc
\param [in] tc tableau de pointeur sur des Candidat
\param [in] tl taille logique du tableau tc
\param [in] numc numéro de candidat
\param [in] trouve pointeur vers un int qui permet de dire après l'execution de cette fonction si le candidat existe dans tc
\return Candidat** tableau de pointeur sur des Candidat
*/
int recherche_candidat(Candidat **tc, int numc,int tl,int *trouve)
{
    int inf = 0, sup = tl - 1, m;
    while (sup >= inf){
        m = (sup+inf)/2;
        if (numc == tc[m]->numc) { *trouve = 1; return m; }
        if (numc < tc[m]->numc) inf = m + 1;
        else sup = m - 1;
    }
    *trouve = 0;
    return inf;
}
/**
\brief ajoute un choix dans la liste chainé de type maillonChoix
\param [out] lchoix liste chainé de type maillonChoix
\param [in] lchoix liste chainé de type maillonChoix
\param [in] ville nom de la ville
\parma [in] dep nom du département
\param [in] desdp int représentant la décision du département
\param [in] valcan int représentant la validation du candidat 
\return ListeChoix liste chainé de type maillonChoix
*/
ListeChoix ajouter_choix(ListeChoix lchoix,char ville[31], char dep[31], int desdp, int valcan)
{
    if (lchoix == NULL) return ajouter_choix_en_tete(lchoix,ville,dep,desdp,valcan);
    if (strcmp(lchoix->ville,ville) < 0)
    {
        lchoix = ajouter_choix_en_tete(lchoix,ville,dep,desdp,valcan);
        return lchoix;
    }
    if (strcmp(lchoix->ville,ville) == 0 && strcmp(lchoix->dep,dep) == 0) 
    {
        lchoix->valcan = valcan;
        return lchoix;
    }
    if (strcmp(lchoix->ville,ville) == 0) return ajouter_choix_en_tete(lchoix,ville,dep,desdp,valcan);
    lchoix->suiv = ajouter_choix(lchoix->suiv,ville,dep,desdp,valcan);
    return lchoix;
}
/**
\brief supprime un choix dans la liste chainé de type maillonChoix
\param [out] lchoix liste chainé de type maillonChoix
\param [in] lchoix liste chainé de type maillonChoix
\param [in] ville nom de la ville
\parma [in] dep nom du département
\return ListeChoix liste chainé de type maillonChoix
*/
ListeChoix supprimer_choix(ListeChoix lchoix,char ville[31], char dep[31])
{
    if (lchoix == NULL) stop("un uttilisateur ne peux supprimer une candidature qui n'eciste pas");
    if (strcmp(lchoix->ville,ville) < 0) stop("un uttilisateur ne peux supprimer une candidature qui n'eciste pas");
    if (strcmp(lchoix->ville,ville) == 0 && strcmp(lchoix->dep,dep) == 0) return supprimer_choix_en_tete(lchoix);
    lchoix->suiv = supprimer_choix(lchoix->suiv,ville,dep);
    return lchoix;
}
/**
\brief ajoute un choix en tête dans la liste chainé de type maillonChoix
\param [out] lchoix liste chainé de type maillonChoix
\param [in] lchoix liste chainé de type maillonChoix
\param [in] ville nom de la ville
\parma [in] dep nom du département
\param [in] desdp int représentant la décision du département
\param [in] valcan int représentant la validation du candidat 
\return ListeChoix liste chainé de type maillonChoix
*/
ListeChoix ajouter_choix_en_tete(ListeChoix lchoix,char ville[31],char dep[31], int desdp, int valcan)
{
    maillonChoix *x = (maillonChoix *) malloc(sizeof(maillonChoix));
    if (x == NULL) stop("problème de malloc dans ajouter_choix_en_tete (struct.c)");
    strcpy(x->ville,ville);
    strcpy(x->dep,dep);
    x->desdp = desdp;
    x->valcan = valcan;
    x->suiv = lchoix;
    return x;
}
/**
\brief supprime un choix en tête dans la liste chainé de type maillonChoix
\param [out] lchoix liste chainé de type maillonChoix
\param [in] lchoix liste chainé de type maillonChoix
\return ListeChoix liste chainé de type maillonChoix
*/
ListeChoix supprimer_choix_en_tete(ListeChoix lchoix)
{
    maillonChoix *x = lchoix->suiv;
    free(lchoix);
    return x;
}
/**
\brief affiche d'une manière itérative tout les étudiants
\param [out] void void
\param [in] tc tableau de pointeur vers des structures de type Candidat
\param [in] tlc taille logique du tableau tc
\return void
*/
void affiche_tout_candidat(Candidat** tc, int tlc)
{
    clear();
    printf("n°candidat\tnom\tprenom\t\t\tnote\t\t\t\t\tnombre de veux\n");
    for (int i=0; i<tlc; i++)
    {
        affiche_candidat(*tc[i]);
    }
}
/**
\brief permet d'afficher d'une manière récursive tout les étudiants
\param [out] void void
\param [in] ldept liste chainé de département
\param [in] dep nom du département
\return void
*/
void affiche_candidat_departement_ldept(ListeDept ldept,char dep[31])
{
    if (ldept == NULL) return;
    if (strcmp(ldept->nom,dep) == 0) 
    {
        printf("\nnom\tn°candidat\n");
        affiche_demande(ldept->dem);
        return;
    }
    if (strcmp(ldept->nom,dep) < 0) 
    {
        printf("\nce département n'existe pas\n");
        return;
    }
    affiche_candidat_departement_ldept(ldept->suiv,dep);
}
/**
\brief permet d'afficher d'une manière récursive toutes les demandes
\param [out] void void
\param [in] ldept liste chainé de département
\param [in] dep nom du département
\param [in] nom nom du candidat
\param [in] numc numéro de candidat
\param [in] desdp int représentant la décision du département
\param [in] valcan int représentant la validation du candidat 
\return void
*/
void ajouter_demande_ldept(ListeDept ldept,char dep[31], char nom[31], int numc, int desdp, int valcan)
{
    if (ldept == NULL) stop("un étudiant à postuler pour un département qui n'existe pas");
    if (strcmp(ldept->nom,dep) == 0)
    {
        if (ldept->dem == NULL) ldept->dem = ajouter_demande_en_tete(ldept->dem,nom,numc,desdp,valcan);
        else ldept->dem->prec = ajouter_demande(ldept->dem->prec, nom, numc, desdp, valcan);
        return;
    }
    if (strcmp(ldept->nom,dep) < 0) stop("un étudiant à postuler pour un département qui n'existe pas");
    ajouter_demande_ldept(ldept->suiv,dep,nom,numc,desdp,valcan);
}
/**
\brief supprime une demande d'une liste chainé de type ListeDept
\param [out] void void
\param [in] ldept liste chainé de département
\param [in] dep nom du département
\param [in] nom nom du candidat
\return void
*/
void supprimer_demande_ldept(ListeDept ldept,char dep[31], char nom[31])
{
    if (ldept == NULL) stop("un étudiant à supprimer ça candidature pour un département qui n'existe pas");
    if (strcmp(ldept->nom,dep) == 0)
    {
        if (ldept->dem == NULL) ldept->dem = supprimer_demande_en_tete(ldept->dem);
        else ldept->dem->prec = supprimer_demande(ldept->dem->prec, nom);
        return;
    }
    if (strcmp(ldept->nom,dep) < 0) stop("un étudiant à postuler pour un département qui n'existe pas");
    supprimer_demande_en_tete(ldept->dem);
}
/**
\brief ajoute une demande dans une liste chainé de type demande*
\param [out] demande* liste chainé de structure de type demande
\param [in] dem structure de type demande
\param [in] dep nom du département
\param [in] nom nom du candidat
\param [in] numc numéro de candidat
\param [in] desdp int représentant la décision du département
\param [in] valcan int représentant la validation du candidat 
\return demande* liste chainé de structure de type demande
*/
demande* ajouter_demande(demande *dem, char nom[31], int numc, int desdp, int valcan)
{
    if (dem == NULL) return ajouter_demande_en_tete(dem,nom,numc,desdp,valcan);
    if (strcmp(dem->nom,nom) == 0) stop("un candidat ne peut pas postuler deux fois dans le même département");
    if (strcmp(dem->nom,nom) < 0) return ajouter_demande_en_tete(dem,nom,numc,desdp,valcan);
    ajouter_demande(dem->prec,nom,numc,desdp,valcan);
}
/**
\brief supprime une demande d'une liste chainé de type demande
\param [out] dem liste chainé de type demande
\param [in] dem liste chainé de structure de type demande
\param [in] dep nom du département
\param [in] nom nom du candidat
\return demande* liste chainé de structure de type demande
*/
demande* supprimer_demande(demande *dem, char nom[31])
{
    if (dem == NULL) stop("un candidat ne peut annulé une candidature qui n'existe pas");
    if (strcmp(dem->nom,nom) == 0) return supprimer_demande_en_tete(dem);
    supprimer_demande(dem->prec,nom);
}
/**
\brief ajoute une demande en tête dans une liste chainé de type demande*
\param [out] demande* liste chainé de structure de type demande
\param [in] dem structure de type demande
\param [in] dep nom du département
\param [in] nom nom du candidat
\param [in] numc numéro de candidat
\param [in] desdp int représentant la décision du département
\param [in] valcan int représentant la validation du candidat 
\return demande* liste chainé de structure de type demande
*/
demande* ajouter_demande_en_tete(demande *dem, char nom[31], int numc, int desdp, int valcan)
{
    demande *x = (demande *) malloc(sizeof(demande));
    if (x == NULL) stop("problème de malloc dans ajouter_demande_en_tete (struct.c)");
    strcpy(x->nom,nom);
    x->numc = numc;
    x->desdp = desdp;
    x->valcan = valcan;
    if (dem == NULL)
    {
        x->prec = NULL;
        dem = x;
        return dem;
    }
    x->prec = dem->prec;
    dem->prec = x;
    return dem;
}
/**
\brief supprime la demande en tête d'une liste chainé de type demande
\param [out] dem liste chainé de type demande
\param [in] dem liste chainé de structure de type demande
\return demande* liste chainé de structure de type demande
*/
demande* supprimer_demande_en_tete(demande *dem)
{
    demande *x = dem->prec;
    free(dem);
    return x;
}
/**
\brief vérifie si une demande existe dans une liste chainé de type ListeChoix
\param [out] int 0 si elle n'existe pas et 1 si elle existe
\param [in] dem liste chainé de structure de type demande
\param [in] dep nom du département
\param [in] nom nom du candidat
\return int 0 si elle n'existe pas et 1 si elle existe
*/
int demande_existe(ListeChoix l, char ville[31], char dep[31])
{
    if (l == NULL) return 0;
    if (strcmp(l->ville,ville) == 0 && strcmp(l->dep,dep) == 0) return 1;
    return demande_existe(l->suiv,ville,dep);
}