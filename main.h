/**
*\file main.h
*\brief main.h déclare les fonctions contenue dans le fichier main.c et import les librery stdio/stdlib/string
*\author Ludovic CASTIGLIA
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void clear(void);
void stop(char *erreur);

VilleIUT** lire_iut(char fichier[], int *tl);
void read_line_iut(char ville[31], char departement[31], int *nbpers, char nom[31], FILE *file);
void save_iut(VilleIUT *tiut[], int tl);
void save_line_iut(FILE *file, ListeDept ldept, char ville[31]);

void affiche_departement(VilleIUT *tiut[], int tl);
void affiche_nb_place(VilleIUT *tiut[], int tl);
void affiche_iut(VilleIUT *tiut[], int tl);

void cree_departement(VilleIUT *tiut[], int tl);
void suppr_departement(VilleIUT *tiut[], int tl);
void modif_nom_departement(VilleIUT *tiut[], int tl);
void phase_candidature(void);


Candidat** read_candidat(char fichier[],int *tl);
void read_line_candidat(int *numc, float note[4], char nom[31], char prenom[31], int *nbchoix, FILE *file);
void read_line_demande(char ville[31], char dep[31], int *desdp, int *valcan,FILE *file);
Candidat* lire_candidat(char fichier[],int *tl);
void save_candidat(Candidat *tc[], int tlc);
void charger_demande(VilleIUT *tiut[],int tl, Candidat *tc[], int tlc);
void save_line_candidat(FILE *file, int numc, char nom[31], char prenom[31], float note[4], int nbchoix);
void save_line_demande(FILE *file, char ville[31], char dep[31], int desdp, int valcan);