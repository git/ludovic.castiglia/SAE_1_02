/**
*\file menu.h
*\brief menu.h déclare les fonctions contenue dans le fichier menu.c
*\author Ludovic CASTIGLIA
*/

void global(void);
void merci(void);
int re_menu(void);

int menu_utilisateur(void);
void menu_utilisateur_affiche(void);

int sous_menu_utilisateur(void);
void sous_menu_utilisateur_affiche(void);

int sous_menu_admin(void);
void sous_menu_admin_affiche(void);

int sous_menu_responsable(void);
void sous_menu_responsable_affiche(void);